﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.CSharp;
using System.Diagnostics;
using System.CodeDom.Compiler;

namespace CSCompiler
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion" , framework_version_tbx.Text} });
            CompilerParameters parameters = new CompilerParameters(new string[] { "mscorlib.dll", "System.Core.dll"}, project_name_tbx.Text, true);
            parameters.GenerateExecutable = true;
            CompilerResults results = provider.CompileAssemblyFromSource(parameters, code_box.Text);

            if (results.Errors.HasErrors)
            {
                foreach(CompilerError error in results.Errors.Cast<CompilerError>())
                {
                    status_box.Text += $"Строка {error.Line}:    {error.ErrorText}";
                }
            }
            else
            {
                status_box.Text = "---Сборка завершена---";
            }
            Process.Start($"{Application.StartupPath}/{project_name_tbx.Text}");
        }
    }
}
